<?php

/* book/index.html.twig */
class __TwigTemplate_a864340ea131b3fb4056b7627760f9d5aafdf14a7a722a4d78897e06bc404ebd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "book/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0c54219228b57f7fd9b5f464d6feeba49d701f50d8e3468b04bf6f592d84c5d0 = $this->env->getExtension("native_profiler");
        $__internal_0c54219228b57f7fd9b5f464d6feeba49d701f50d8e3468b04bf6f592d84c5d0->enter($__internal_0c54219228b57f7fd9b5f464d6feeba49d701f50d8e3468b04bf6f592d84c5d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "book/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0c54219228b57f7fd9b5f464d6feeba49d701f50d8e3468b04bf6f592d84c5d0->leave($__internal_0c54219228b57f7fd9b5f464d6feeba49d701f50d8e3468b04bf6f592d84c5d0_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_1424decc150376e2cf43a7f72e9e75c3ec311fd30d6de4d8867cb4d6e69d9f03 = $this->env->getExtension("native_profiler");
        $__internal_1424decc150376e2cf43a7f72e9e75c3ec311fd30d6de4d8867cb4d6e69d9f03->enter($__internal_1424decc150376e2cf43a7f72e9e75c3ec311fd30d6de4d8867cb4d6e69d9f03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"row\">
    <div class=\"col-lg-12\">
        <h1>Listado de libros técnicos de programación</h1>

        <table class=\"table books-table\">
            <thead>
            <th>Portada</th>
            <th>Título</th>
            <th>Autor</th>
            <th>Precio</th>
            <th>Acciones</th>
            </thead>
            <tbody>
  ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["books"]) ? $context["books"] : $this->getContext($context, "books")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["book"]) {
            // line 18
            echo "                <tr>
                    <td><img class=\"img-rounded\" src=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["book"], "coverUrl", array()), "html", null, true);
            echo "\"></td>
                    <td>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["book"], "title", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["book"], "author", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["book"], "price", array()), "html", null, true);
            echo "€</td>
                 <td class=\"actions\">
    <a href=\"\" class=\"btn btn-sm btn-default\">
        <span class=\"glyphicon glyphicon-edit\"></span> Editar
    </a> 
    <a href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("delete_book", array("id" => $this->getAttribute($context["book"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-sm btn-danger\">
        <span class=\"glyphicon glyphicon-trash\"></span> Eliminar
    </a>                         
</td>
                </tr>
    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 33
            echo "                <tr td colspan=\"5\">No se han encontrado libros</td></tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['book'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "            </tbody>
        </table>
    </div>
</div>
";
        
        $__internal_1424decc150376e2cf43a7f72e9e75c3ec311fd30d6de4d8867cb4d6e69d9f03->leave($__internal_1424decc150376e2cf43a7f72e9e75c3ec311fd30d6de4d8867cb4d6e69d9f03_prof);

    }

    public function getTemplateName()
    {
        return "book/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 35,  94 => 33,  83 => 27,  75 => 22,  71 => 21,  67 => 20,  63 => 19,  60 => 18,  55 => 17,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/* <div class="row">*/
/*     <div class="col-lg-12">*/
/*         <h1>Listado de libros técnicos de programación</h1>*/
/* */
/*         <table class="table books-table">*/
/*             <thead>*/
/*             <th>Portada</th>*/
/*             <th>Título</th>*/
/*             <th>Autor</th>*/
/*             <th>Precio</th>*/
/*             <th>Acciones</th>*/
/*             </thead>*/
/*             <tbody>*/
/*   {% for book in books %}*/
/*                 <tr>*/
/*                     <td><img class="img-rounded" src="{{ book.coverUrl }}"></td>*/
/*                     <td>{{ book.title }}</td>*/
/*                     <td>{{ book.author }}</td>*/
/*                     <td>{{ book.price }}€</td>*/
/*                  <td class="actions">*/
/*     <a href="" class="btn btn-sm btn-default">*/
/*         <span class="glyphicon glyphicon-edit"></span> Editar*/
/*     </a> */
/*     <a href="{{ path('delete_book', {'id': book.id}) }}" class="btn btn-sm btn-danger">*/
/*         <span class="glyphicon glyphicon-trash"></span> Eliminar*/
/*     </a>                         */
/* </td>*/
/*                 </tr>*/
/*     {% else %}*/
/*                 <tr td colspan="5">No se han encontrado libros</td></tr>*/
/*     {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
