<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_dd71925f7a53fb4d9d5880943c2b7efae14da46bc19f63be9691d0c6de9e799c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d71998a0f7f481dd0b26806bb50e03faebe8638d97752afae817331734f90fbf = $this->env->getExtension("native_profiler");
        $__internal_d71998a0f7f481dd0b26806bb50e03faebe8638d97752afae817331734f90fbf->enter($__internal_d71998a0f7f481dd0b26806bb50e03faebe8638d97752afae817331734f90fbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d71998a0f7f481dd0b26806bb50e03faebe8638d97752afae817331734f90fbf->leave($__internal_d71998a0f7f481dd0b26806bb50e03faebe8638d97752afae817331734f90fbf_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_219c122a14402c5795cab2af60968ca2d8b07a472d64049d931ae44f20de2270 = $this->env->getExtension("native_profiler");
        $__internal_219c122a14402c5795cab2af60968ca2d8b07a472d64049d931ae44f20de2270->enter($__internal_219c122a14402c5795cab2af60968ca2d8b07a472d64049d931ae44f20de2270_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_219c122a14402c5795cab2af60968ca2d8b07a472d64049d931ae44f20de2270->leave($__internal_219c122a14402c5795cab2af60968ca2d8b07a472d64049d931ae44f20de2270_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_6e3e2c6d9bbfda3512d69dfe273c4494c2969b9aff4486d324d1d1d45bff8b97 = $this->env->getExtension("native_profiler");
        $__internal_6e3e2c6d9bbfda3512d69dfe273c4494c2969b9aff4486d324d1d1d45bff8b97->enter($__internal_6e3e2c6d9bbfda3512d69dfe273c4494c2969b9aff4486d324d1d1d45bff8b97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_6e3e2c6d9bbfda3512d69dfe273c4494c2969b9aff4486d324d1d1d45bff8b97->leave($__internal_6e3e2c6d9bbfda3512d69dfe273c4494c2969b9aff4486d324d1d1d45bff8b97_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_8eeb08d82a7d4c826d959cdb9a17e918e89529e98518bcb97e01781a188d5cf4 = $this->env->getExtension("native_profiler");
        $__internal_8eeb08d82a7d4c826d959cdb9a17e918e89529e98518bcb97e01781a188d5cf4->enter($__internal_8eeb08d82a7d4c826d959cdb9a17e918e89529e98518bcb97e01781a188d5cf4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_8eeb08d82a7d4c826d959cdb9a17e918e89529e98518bcb97e01781a188d5cf4->leave($__internal_8eeb08d82a7d4c826d959cdb9a17e918e89529e98518bcb97e01781a188d5cf4_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
