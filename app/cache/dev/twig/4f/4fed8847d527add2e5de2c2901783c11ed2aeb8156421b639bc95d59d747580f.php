<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_3f5aa00ef73238ba5e3d3c4e3ac96c529d16990eac9ddd41cb4ccb49c7331ce8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_44873f01be6e3135b28fa6d728361328c12d92d35d32c3b52c2aba92636cd28f = $this->env->getExtension("native_profiler");
        $__internal_44873f01be6e3135b28fa6d728361328c12d92d35d32c3b52c2aba92636cd28f->enter($__internal_44873f01be6e3135b28fa6d728361328c12d92d35d32c3b52c2aba92636cd28f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_44873f01be6e3135b28fa6d728361328c12d92d35d32c3b52c2aba92636cd28f->leave($__internal_44873f01be6e3135b28fa6d728361328c12d92d35d32c3b52c2aba92636cd28f_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_b823a31877eebbf84569df53fd65cdc65f1cf8690a2d4fe458573fd1509e871d = $this->env->getExtension("native_profiler");
        $__internal_b823a31877eebbf84569df53fd65cdc65f1cf8690a2d4fe458573fd1509e871d->enter($__internal_b823a31877eebbf84569df53fd65cdc65f1cf8690a2d4fe458573fd1509e871d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_b823a31877eebbf84569df53fd65cdc65f1cf8690a2d4fe458573fd1509e871d->leave($__internal_b823a31877eebbf84569df53fd65cdc65f1cf8690a2d4fe458573fd1509e871d_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_9a88019d7892d81dd0b1117411ae10f4e1eec13814e7bc7ab05b9ee893939ff0 = $this->env->getExtension("native_profiler");
        $__internal_9a88019d7892d81dd0b1117411ae10f4e1eec13814e7bc7ab05b9ee893939ff0->enter($__internal_9a88019d7892d81dd0b1117411ae10f4e1eec13814e7bc7ab05b9ee893939ff0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_9a88019d7892d81dd0b1117411ae10f4e1eec13814e7bc7ab05b9ee893939ff0->leave($__internal_9a88019d7892d81dd0b1117411ae10f4e1eec13814e7bc7ab05b9ee893939ff0_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_125f07206978d89533f9e79d582b3d8e293f0b0310af4247fc4446bee094c6c2 = $this->env->getExtension("native_profiler");
        $__internal_125f07206978d89533f9e79d582b3d8e293f0b0310af4247fc4446bee094c6c2->enter($__internal_125f07206978d89533f9e79d582b3d8e293f0b0310af4247fc4446bee094c6c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_125f07206978d89533f9e79d582b3d8e293f0b0310af4247fc4446bee094c6c2->leave($__internal_125f07206978d89533f9e79d582b3d8e293f0b0310af4247fc4446bee094c6c2_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
