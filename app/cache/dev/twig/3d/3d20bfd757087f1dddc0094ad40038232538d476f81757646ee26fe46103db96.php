<?php

/* book/create.html.twig */
class __TwigTemplate_e7c1b1fb685088f8609c5934ed3cca9b99375e7484983b63e9dbeb88c2613961 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "book/create.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f7ee3539331d220d025cf9ba1ffea0fd1dfe94a2e5583ea9b3a5dcf1d3b82d0f = $this->env->getExtension("native_profiler");
        $__internal_f7ee3539331d220d025cf9ba1ffea0fd1dfe94a2e5583ea9b3a5dcf1d3b82d0f->enter($__internal_f7ee3539331d220d025cf9ba1ffea0fd1dfe94a2e5583ea9b3a5dcf1d3b82d0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "book/create.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f7ee3539331d220d025cf9ba1ffea0fd1dfe94a2e5583ea9b3a5dcf1d3b82d0f->leave($__internal_f7ee3539331d220d025cf9ba1ffea0fd1dfe94a2e5583ea9b3a5dcf1d3b82d0f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_61ab2c08c31f3c7ed8fcf85b302036759945b50debfdbfec12d2a07861c4a153 = $this->env->getExtension("native_profiler");
        $__internal_61ab2c08c31f3c7ed8fcf85b302036759945b50debfdbfec12d2a07861c4a153->enter($__internal_61ab2c08c31f3c7ed8fcf85b302036759945b50debfdbfec12d2a07861c4a153_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<form class=\"form-horizontal\" method=\"post\" action=\"";
        echo $this->env->getExtension('routing')->getPath("new_book");
        echo "\">
    <div class=\"form-group\">
        <label for=\"title\" class=\"col-sm-2 control-label\">Título</label>
        <div class=\"col-sm-10\">
            <input type=\"text\" class=\"form-control\" name=\"title\" id=\"title\" placeholder=\"Titulo\">
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"cover\" class=\"col-sm-2 control-label\">Portada</label>
        <div class=\"col-sm-10\">
            <input type=\"url\" class=\"form-control\" id=\"cover\" name=\"cover\" placeholder=\"URL de la portada\">
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"author\" class=\"col-sm-2 control-label\">Autor</label>
        <div class=\"col-sm-10\">
            <input type=\"text\" class=\"form-control\" id=\"author\" name=\"author\" placeholder=\"Autor\">
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"price\" class=\"col-sm-2 control-label\">Precio</label>
        <div class=\"col-sm-10\">
            <input type=\"number\" class=\"form-control\" id=\"price\" name=\"price\" placeholder=\"Precio\">
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"col-sm-offset-2 col-sm-10\">
            <button type=\"submit\" class=\"btn btn-success pull-right\">Crear</button>
        </div>
    </div>
</form>

";
        
        $__internal_61ab2c08c31f3c7ed8fcf85b302036759945b50debfdbfec12d2a07861c4a153->leave($__internal_61ab2c08c31f3c7ed8fcf85b302036759945b50debfdbfec12d2a07861c4a153_prof);

    }

    public function getTemplateName()
    {
        return "book/create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/* <form class="form-horizontal" method="post" action="{{ path('new_book') }}">*/
/*     <div class="form-group">*/
/*         <label for="title" class="col-sm-2 control-label">Título</label>*/
/*         <div class="col-sm-10">*/
/*             <input type="text" class="form-control" name="title" id="title" placeholder="Titulo">*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label for="cover" class="col-sm-2 control-label">Portada</label>*/
/*         <div class="col-sm-10">*/
/*             <input type="url" class="form-control" id="cover" name="cover" placeholder="URL de la portada">*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label for="author" class="col-sm-2 control-label">Autor</label>*/
/*         <div class="col-sm-10">*/
/*             <input type="text" class="form-control" id="author" name="author" placeholder="Autor">*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label for="price" class="col-sm-2 control-label">Precio</label>*/
/*         <div class="col-sm-10">*/
/*             <input type="number" class="form-control" id="price" name="price" placeholder="Precio">*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <div class="col-sm-offset-2 col-sm-10">*/
/*             <button type="submit" class="btn btn-success pull-right">Crear</button>*/
/*         </div>*/
/*     </div>*/
/* </form>*/
/* */
/* {% endblock %}*/
