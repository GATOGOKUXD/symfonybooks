<?php

/* base.html.twig */
class __TwigTemplate_544028ed5be310805e9b7e802b25631c81ed852934bd72004e171401c6c9ffe5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_25922f3f52de0707b30af083a2631a302406d0e1fb81e93c1cd64e66728bb24a = $this->env->getExtension("native_profiler");
        $__internal_25922f3f52de0707b30af083a2631a302406d0e1fb81e93c1cd64e66728bb24a->enter($__internal_25922f3f52de0707b30af083a2631a302406d0e1fb81e93c1cd64e66728bb24a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "

<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 11
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/main.css"), "html", null, true);
        echo "\">
    </head>
    <body>

           <nav class=\"navbar navbar-inverse\" role=\"navigation\"> 
            <div class=\"navbar-header\">                
                <a class=\"navbar-brand\" href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("homepage");
        echo "\">Librería 4V</a>
            </div>
            <ul class=\"nav navbar-nav\">
                <li class=\"active\"><a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("create_action");
        echo "\">Añadir un libro</a></li>
            </ul>
            <form class=\"navbar-form navbar-right\" role=\"search\" 
                  action=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("search_books");
        echo "\">
                <div class=\"input-group\">
                    <input type=\"text\" class=\"form-control\" placeholder=\"Buscar...\" 
                        name=\"searchTerm\" id=\"srch-term\">
                    <div class=\"input-group-btn\">
                        <button class=\"btn btn-default\" type=\"submit\">
                            <i class=\"glyphicon glyphicon-search\"></i></button>
                    </div>
                </div>
            </form>
        </nav>
        
   

        <!-- Page Content -->
        <div class=\"container\">
          ";
        // line 40
        $this->displayBlock('body', $context, $blocks);
        // line 42
        echo "        </div>

        ";
        // line 44
        $this->displayBlock('javascripts', $context, $blocks);
        // line 45
        echo "    </body>
</html>
";
        
        $__internal_25922f3f52de0707b30af083a2631a302406d0e1fb81e93c1cd64e66728bb24a->leave($__internal_25922f3f52de0707b30af083a2631a302406d0e1fb81e93c1cd64e66728bb24a_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_7b470f9c74c2b1958bcb77b8f0b120474b2288db8e1876f5c316d7478590877c = $this->env->getExtension("native_profiler");
        $__internal_7b470f9c74c2b1958bcb77b8f0b120474b2288db8e1876f5c316d7478590877c->enter($__internal_7b470f9c74c2b1958bcb77b8f0b120474b2288db8e1876f5c316d7478590877c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_7b470f9c74c2b1958bcb77b8f0b120474b2288db8e1876f5c316d7478590877c->leave($__internal_7b470f9c74c2b1958bcb77b8f0b120474b2288db8e1876f5c316d7478590877c_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_c33fcd42239a1aa8442058343de6ba98871f5d841aa8a9076efcfaef21cdbfd9 = $this->env->getExtension("native_profiler");
        $__internal_c33fcd42239a1aa8442058343de6ba98871f5d841aa8a9076efcfaef21cdbfd9->enter($__internal_c33fcd42239a1aa8442058343de6ba98871f5d841aa8a9076efcfaef21cdbfd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 9
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
        ";
        
        $__internal_c33fcd42239a1aa8442058343de6ba98871f5d841aa8a9076efcfaef21cdbfd9->leave($__internal_c33fcd42239a1aa8442058343de6ba98871f5d841aa8a9076efcfaef21cdbfd9_prof);

    }

    // line 40
    public function block_body($context, array $blocks = array())
    {
        $__internal_2ef1cca0ebc9ba75efba7f7e520590eab08ce2e8306e4263d2177af16cfb376c = $this->env->getExtension("native_profiler");
        $__internal_2ef1cca0ebc9ba75efba7f7e520590eab08ce2e8306e4263d2177af16cfb376c->enter($__internal_2ef1cca0ebc9ba75efba7f7e520590eab08ce2e8306e4263d2177af16cfb376c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 41
        echo "          ";
        
        $__internal_2ef1cca0ebc9ba75efba7f7e520590eab08ce2e8306e4263d2177af16cfb376c->leave($__internal_2ef1cca0ebc9ba75efba7f7e520590eab08ce2e8306e4263d2177af16cfb376c_prof);

    }

    // line 44
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_4f6b2a6fd3f47a176e9f1ba734a8298af024430e522bf0b1ac0c01bc7946bce6 = $this->env->getExtension("native_profiler");
        $__internal_4f6b2a6fd3f47a176e9f1ba734a8298af024430e522bf0b1ac0c01bc7946bce6->enter($__internal_4f6b2a6fd3f47a176e9f1ba734a8298af024430e522bf0b1ac0c01bc7946bce6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_4f6b2a6fd3f47a176e9f1ba734a8298af024430e522bf0b1ac0c01bc7946bce6->leave($__internal_4f6b2a6fd3f47a176e9f1ba734a8298af024430e522bf0b1ac0c01bc7946bce6_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 44,  136 => 41,  130 => 40,  120 => 9,  114 => 8,  102 => 7,  93 => 45,  91 => 44,  87 => 42,  85 => 40,  66 => 24,  60 => 21,  54 => 18,  45 => 12,  40 => 11,  38 => 8,  34 => 7,  26 => 1,);
    }
}
/* */
/* */
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}*/
/*         <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">*/
/*         {% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*         <link rel="stylesheet" href="{{ asset('css/main.css') }}">*/
/*     </head>*/
/*     <body>*/
/* */
/*            <nav class="navbar navbar-inverse" role="navigation"> */
/*             <div class="navbar-header">                */
/*                 <a class="navbar-brand" href="{{ path('homepage') }}">Librería 4V</a>*/
/*             </div>*/
/*             <ul class="nav navbar-nav">*/
/*                 <li class="active"><a href="{{ path('create_action') }}">Añadir un libro</a></li>*/
/*             </ul>*/
/*             <form class="navbar-form navbar-right" role="search" */
/*                   action="{{ path('search_books') }}">*/
/*                 <div class="input-group">*/
/*                     <input type="text" class="form-control" placeholder="Buscar..." */
/*                         name="searchTerm" id="srch-term">*/
/*                     <div class="input-group-btn">*/
/*                         <button class="btn btn-default" type="submit">*/
/*                             <i class="glyphicon glyphicon-search"></i></button>*/
/*                     </div>*/
/*                 </div>*/
/*             </form>*/
/*         </nav>*/
/*         */
/*    */
/* */
/*         <!-- Page Content -->*/
/*         <div class="container">*/
/*           {% block body %}*/
/*           {% endblock %}*/
/*         </div>*/
/* */
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
