<?php

/* @Security/Collector/icon.svg */
class __TwigTemplate_1e45f3ca92d5f81b38d4eb586d8dbe400aab8de0a981b045b7c28c07fa214580 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_28d6339f7c76d25be05d2ade6d77ff20ac6cf0b615bda0c293e86d8b2b1a9904 = $this->env->getExtension("native_profiler");
        $__internal_28d6339f7c76d25be05d2ade6d77ff20ac6cf0b615bda0c293e86d8b2b1a9904->enter($__internal_28d6339f7c76d25be05d2ade6d77ff20ac6cf0b615bda0c293e86d8b2b1a9904_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
        
        $__internal_28d6339f7c76d25be05d2ade6d77ff20ac6cf0b615bda0c293e86d8b2b1a9904->leave($__internal_28d6339f7c76d25be05d2ade6d77ff20ac6cf0b615bda0c293e86d8b2b1a9904_prof);

    }

    public function getTemplateName()
    {
        return "@Security/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" height="24" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/*     <path fill="#AAAAAA" d="M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z"/>*/
/* </svg>*/
/* */
